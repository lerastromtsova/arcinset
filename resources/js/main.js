function load(){
    userLang = navigator.language.slice(0,2);
    console.log(userLang);

    var translate = new Translate();
    var attributeName = 'data-tag';
    translate.init(attributeName, userLang);
    translate.process();
}

function changeLang(){
    if (userLang == 'ru') {
        userLang = 'en'
    }
    else {
        userLang = 'ru'
    }

    var translate = new Translate();
    var attributeName = 'data-tag';
    translate.init(attributeName, userLang);
    translate.process();
}
